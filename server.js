#! /usr/bin/node

'use strict';

var express = require('express'),
    app = express(),
    bodyParser = require('body-parser');

const PORT = 9999;

app.use(bodyParser.json({limit: '500kb'}));

require('./lib/routes.js')(app);

app.addListener('request', app);

app.listen(PORT);

console.log(`Listening on port ${PORT}`);

