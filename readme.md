bringg-task
===========

Before running the server, please update your node to version 4.4.x, and install the dependencies by running:
```bash
npm install
```

Run the server by running:
```bash
npm start
```

The server listens on port 9999, and exposes the following endpoints:
```
POST /api/orders
```
Expects the following body:
```javascript
{
   "name": "My Name",
   "phone": "+97212345",
   "address": "My address"
}
```

And:
```
GET /api/orders?phone="+97212345"
```
