module.exports = function(app) {
    var logic = require('./logic');

    const SERVER_ERROR = 500,
          NOT_FOUND = 404;

    function responseWithJson(response) {
        return function(err, results) {
            if (err) {
                console.error(err);
                response.status(err.httpCode || SERVER_ERROR).send(err.message);
                return;
            }

            if (!results) {
                response.sendStatus(NOT_FOUND);
                return;
            }

            response.json(results);
        };
    }

    app.post('/api/orders', (req, res) => {
        logic.createOrder(req.body, responseWithJson(res));
    });

    app.get('/api/orders', (req, res) => {
        logic.getOrders(req.query.phone, responseWithJson(res));
    });
};
