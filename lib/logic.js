'use strict';

var request = require('request'),
    crypto = require('crypto'),
    async = require('async');

const COMPANY_ID = 10338,
      ACCESS_TOKEN = 'gsy9eZsp6GKjtawks17T', // In a real scenario, I'd read these keys from an encrypted file
      SECRET_KEY = 'XPQFqqgB4EbG_MNqWrkG',
      PAGE_SIZE = 50;

module.exports = {
    createOrder: function (order, done) {
        if(!verifyOrder(order)) {
            done(new Error('Bad args'));
            return;
        }

        createCustomer(order, (err, res) => {
            if (err) {
                done(err);
                return;
            }

            createTask(res.customer, done);
        });
    },

    getOrders: function (phone, done) {
        if(!phone) {
            done(new Error('Bad args'));
            return;
        }

        let customersOrders = [],
            page = 0,
            reachedLastPage = false,
            weekAgo = Date.now - 1000 * 60 * 60 * 24 * 7;

        // I didn't find any way to send a filter as part of the API, so I'm getting all the tasks and filtering them here. Not very efficient, I know.
        async.doUntil((callback) => {
            callBringgApi({page: page}, `/tasks`, 'GET', (err, tasks) => {
                if(err) {
                    callback(err);
                    return;
                }

                customersOrders = customersOrders.concat(tasks.filter(order => {
                    return order.customer.phone === phone && new Date(order.created_at) >= weekAgo
                }));
                reachedLastPage = tasks.length < PAGE_SIZE;
                page++;
                callback();
            });
        }, () => reachedLastPage, (err) => {
            done(err, customersOrders);
        });
    }
};

function verifyOrder(order) {
    return order.name && order.phone && order.address;// && order.details;
}

function createCustomer(order, done) {
    var params = {
        name: order.name,
        phone: order.phone
    };

    callBringgApi(params, 'customers', 'POST', done);
}

function createTask(customer, done) {
    var params = {
        customer_id: customer.id
    };

    callBringgApi(params, 'tasks', 'POST', done);
}

function createSignature(params) {
    var queryParams = '';
    for (var key in params) {
        var value = params[key];
        if (queryParams.length > 0) {
            queryParams += '&';
        }
        queryParams += key + '=' + encodeURIComponent(value);
    }

    return crypto.createHmac('sha1', SECRET_KEY).update(queryParams).digest('hex');
}

function callBringgApi(params, endpoint, method, done) {
    params.access_token = ACCESS_TOKEN;
    params.timestamp = Date.now();
    params.company_id = COMPANY_ID;
    params.signature = createSignature(params);

    let requestParams = {
        url: `http://developer-api.bringg.com/partner_api/${endpoint}`,
        method: method,
        json: params
    };

    request(requestParams, (err, res, body) => {
        if (err || res.statusCode !== 200) {
            console.log(err || res.statusCode );
            done(err || new Error(`Failed Bringg API call: ${endpoint}`));
            return;
        }

        done(null, body)
    });
}